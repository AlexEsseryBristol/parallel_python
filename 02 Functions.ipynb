{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 1: Functions as Objects\n",
    "\n",
    "Functional programming is based on treating a function in the same way as you would a variable or object. So, to start, we should first create a function. This will be a simple function that just adds together two numbers. Please type in the Python Console:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "def add(x, y):\n",
    "    \"\"\"Simple function returns the sum of the arguments\"\"\"\n",
    "    return x + y"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is a very simple function that just returns the sum of its two arguments. Call the function using, e.g."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "10"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "add(3, 7)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In functional programming, a function is treated in exactly the same way as a variable or an object. This means that a function can be assigned to a variable, e.g. type"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "10"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "a = add\n",
    "a(3, 7)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see the same output. Here, we have assigned the function `add` to the variable `a`. So how does this work?\n",
    "\n",
    "For variables, you should be comfortable with the idea that a variable refers to a piece of data. For example,\n",
    "\n",
    "```python\n",
    "my_num = 10\n",
    "```\n",
    "\n",
    "would create a piece of data (the integer `10`) and will create a variable `my_num` which we use to refer to it. When we type\n",
    "\n",
    "```python\n",
    "a = my_num\n",
    "```\n",
    "\n",
    "we are creating a new variable `a` which points to whatever `my_num` was pointing to. Now both `a` and `my_num` contain (or point to) the same data.\n",
    "\n",
    "For functional programming, the code of a function is also treated like a piece of data. The code\n",
    "\n",
    "```python\n",
    "def add(x, y):\n",
    "    \"\"\"Simple function returns the sum of the arguments\"\"\"\n",
    "    return x + y\n",
    "```\n",
    "\n",
    "creates a new piece of data (the code to add together `x` and `y`), and creates a new name `add` which points to that code. When we then typed\n",
    "\n",
    "```python\n",
    "a = add\n",
    "```\n",
    "\n",
    "we created a new variable `a` which refers to the same piece of code data that `add` pointed to. Now both `a` and `add` or point to the same data, i.e. the same code that adds together the two arguments (e.g. `add(3, 7)` and `a(3, 7)` will call the same code, and give the same result).\n",
    "\n",
    "This means that “function” is a type, in the same way that “integer”, “string” and “floating point number” are types."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Properties of a Function\n",
    "\n",
    "Just as “integer” and “string” have properties, so to does “function”. Type into the Python Console:\n",
    "\n",
    "```python\n",
    "add.__[TAB]\n",
    "```\n",
    "\n",
    "(where `[TAB]` means that you should press the tab key)\n",
    "\n",
    "This should show something like\n",
    "\n",
    "```python\n",
    "add.__call__          add.__dict__          add.__hash__          add.__reduce_ex__\n",
    "add.__class__         add.__doc__           add.__init__          add.__repr__\n",
    "add.__closure__       add.__format__        add.__module__        add.__setattr__\n",
    "add.__code__          add.__get__           add.__name__          add.__sizeof__\n",
    "add.__defaults__      add.__getattribute__  add.__new__           add.__str__\n",
    "add.__delattr__       add.__globals__       add.__reduce__        add.__subclasshook__\n",
    "```\n",
    "\n",
    "(exactly what you see will depend on your version of python)\n",
    "\n",
    "This is the list of properties (functions and variables) of a function. The most interesting variables are `__name__` and `__doc__`. Try typing"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'add'"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "add.__name__"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'Simple function returns the sum of the arguments'"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "add.__doc__"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From the output, can you guess what these two variables contain?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Functions as Arguments\n",
    "\n",
    "As well as assigning functions to variables, you can also pass functions as arguments. Type this into the Console:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "10"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "def call_function(func, arg1, arg2):\n",
    "    \"\"\"\n",
    "    Simple function that calls the function 'func' with  \n",
    "    arguments 'arg1' and 'arg2', returning the result\n",
    "    \"\"\"\n",
    "    return func(arg1, arg2)\n",
    "\n",
    "call_function(add, 3, 7)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Can you see why we get this output?\n",
    "\n",
    "The function `call_function` takes three arguments. The first is the function to be called. The second two arguments are the arguments that will be passed to that function. The code in `call_function` simply calls `func` using the arguments `arg1` and `arg2`. So far, so useless…\n",
    "\n",
    "However, let us now create another function, called `diff`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "def diff(x, y):\n",
    "    \"\"\"\n",
    "    Simple function that returns the difference of\n",
    "    its arguments\n",
    "    \"\"\"\n",
    "    return x - y"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and then type"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "7"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "call_function(diff, 9, 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What has happened here?\n",
    "\n",
    "Now we have passed the function `diff` to `call_function`, and so `func(arg1, arg2)` has used the code contained in `diff`, e.g. calculating the difference of the two numbers.\n",
    "\n",
    "You are probably now wondering how has this helped? Well, let us now change `call_function`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "def call_function(func, arg1, arg2):\n",
    "    \"\"\"\n",
    "    Simple function that returns the difference of\n",
    "    its arguments\n",
    "    \"\"\"\n",
    "    print(f\"Calling function {func.__name__} with arguments {arg1} and {arg2}.\")\n",
    "    result = func(arg1, arg2)\n",
    "    print(f\"The result is {result}\")\n",
    "    return result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Calling function add with arguments 3 and 7.\n",
      "The result is 10\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "10"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "call_function(add, 3, 7)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now try:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Calling function diff with arguments 9 and 2.\n",
      "The result is 7\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "7"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "call_function(diff, 9, 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The new `call_function` is now doing something useful. It is printing out extra information about our functions, and can do that for any function (which accepts two arguments) that we pass. For example, now type"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Calling function multiply with arguments 4 and 5.\n",
      "The result is 20\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "20"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "def multiply(x, y):\n",
    "    \"\"\"\n",
    "    Simple function that returns the prodict of the\n",
    "    two arguments\n",
    "    \"\"\"\n",
    "    return x * y\n",
    "\n",
    "call_function(multiply, 4, 5)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
