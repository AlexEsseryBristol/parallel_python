{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 2: Asynchronous Functions and Futures\n",
    "\n",
    "The `ProcessPoolExecutor.map` function allows you to map a *single* function across an entire list of data. But what if you want to apply lots of *different* functions? The solution is to tell individual workers to run different functions, by *submitting* functions to workers.\n",
    "\n",
    "The `ProcessPoolExecutor` class comes with the function `submit`. This is used to tell one process in the worker pool to run a specified function. For example, create a new script called `pool_submit.py` and type into it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting pool_submit.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile pool_submit.py\n",
    "\n",
    "import os\n",
    "import time\n",
    "from concurrent.futures import ProcessPoolExecutor\n",
    "\n",
    "def slow_function(nsecs):\n",
    "    \"\"\"\n",
    "    Function that sleeps for 'nsecs' seconds, returning\n",
    "    the number of seconds that it slept\n",
    "    \"\"\"\n",
    "\n",
    "    print(f\"Process {os.getpid()} going to sleep for {nsecs} second(s)\")\n",
    "\n",
    "    # use the time.sleep function to sleep for nsecs seconds\n",
    "    time.sleep(nsecs)\n",
    "\n",
    "    print(f\"Process {os.getpid()} waking up\")\n",
    "\n",
    "    return nsecs\n",
    "\n",
    "if __name__ == \"__main__\":\n",
    "    print(f\"Master process is PID {os.getpid()}\")\n",
    "\n",
    "    with ProcessPoolExecutor() as pool:\n",
    "        r = pool.submit(slow_function, 5)\n",
    "\n",
    "    print(f\"Result is {r.result()}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run this script:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Master process is PID 28612\n",
      "Process 28613 going to sleep for 5 second(s)\n",
      "Process 28613 waking up\n",
      "Result is 5\n"
     ]
    }
   ],
   "source": [
    "!venv/bin/python pool_submit.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You should see the something like this printed to the screen (with a delay of five seconds when the worker process sleeps).\n",
    "\n",
    "The key line in this script is:\n",
    "\n",
    "```python\n",
    "r = pool.submit(slow_function, 5)\n",
    "```\n",
    "\n",
    "The `pool.submit` function will request that one of the workers in the pool should run the passed function (in this case `slow_function`), with the arguments passed to the `submit`. The value returned by `pool.submit` is a special kind of object and in order to get the return valule of the function call out of it, we have to call the `result()` method on it. The reasons for this will be covered shortly.\n",
    "\n",
    "The call to `submit` can take multiple arguments, as long as the submitted function takes the same number of arguments. For example, edit your `pool_submit.py` function to read:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting pool_submit.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile pool_submit.py\n",
    "\n",
    "import os\n",
    "import time\n",
    "from concurrent.futures import ProcessPoolExecutor\n",
    "\n",
    "def slow_add(nsecs, x, y):\n",
    "    \"\"\"\n",
    "    Function that sleeps for 'nsecs' seconds, and\n",
    "    then returns the sum of x and y\n",
    "    \"\"\"\n",
    "    print(f\"Process {os.getpid()} going to sleep for {nsecs} second(s)\")\n",
    "\n",
    "    time.sleep(nsecs)\n",
    "\n",
    "    print(f\"Process {os.getpid()} waking up\")\n",
    "\n",
    "    return x + y\n",
    "\n",
    "if __name__ == \"__main__\":\n",
    "    print(f\"Master process is PID {os.getpid()}\")\n",
    "\n",
    "    with ProcessPoolExecutor() as pool:\n",
    "        r = pool.submit(slow_add, 1, 6, 7)\n",
    "\n",
    "    print(f\"Result is {r.result()}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we have edited `slow_function` to be `slow_add`, with this function accepting three arguments. These three arguments are passed using the arguments in `pool.apply(slow_add, 1, 6, 7)`.\n",
    "\n",
    "Running this script using should give output similar to:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Master process is PID 28633\n",
      "Process 28634 going to sleep for 1 second(s)\n",
      "Process 28634 waking up\n",
      "Result is 13\n"
     ]
    }
   ],
   "source": [
    "!venv/bin/python pool_submit.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Asynchronous Functions\n",
    "\n",
    "The reason for the explicit call to `r.result()` is that it allows us to submit multiple functions to run in parallel and to then request their results once they have finished:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting applyasync.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile applyasync.py\n",
    "\n",
    "import os\n",
    "import time\n",
    "from concurrent.futures import ProcessPoolExecutor\n",
    "\n",
    "def slow_add(nsecs, x, y):\n",
    "    \"\"\"\n",
    "    Function that sleeps for 'nsecs' seconds, and\n",
    "    then returns the sum of x and y\n",
    "    \"\"\"\n",
    "    print(f\"Process {os.getpid()} going to sleep for {nsecs} second(s)\")\n",
    "\n",
    "    time.sleep(nsecs)\n",
    "\n",
    "    print(f\"Process {os.getpid()} waking up\")\n",
    "\n",
    "    return x + y\n",
    "\n",
    "if __name__ == \"__main__\":\n",
    "    print(f\"Master process is PID {os.getpid()}\")\n",
    "\n",
    "    with ProcessPoolExecutor() as pool:\n",
    "        r1 = pool.submit(slow_add, 1, 6, 7)\n",
    "        r2 = pool.submit(slow_add, 1, 2, 3)\n",
    "\n",
    "        print(f\"Result one is {r1.result()}\")\n",
    "        print(f\"Result two is {r2.result()}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Running this script using should give output similar to:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "tags": [
     "nbval-ignore-output"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Master process is PID 28667\n",
      "Process 28668 going to sleep for 1 second(s)\n",
      "Process 28669 going to sleep for 1 second(s)\n",
      "Process 28669 waking up\n",
      "Process 28668 waking up\n",
      "Result one is 13\n",
      "Result two is 5\n"
     ]
    }
   ],
   "source": [
    "!venv/bin/python applyasync.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The keys lines of this script are\n",
    "\n",
    "```python\n",
    "r1 = pool.submit(slow_add, 1, 6, 7)\n",
    "r2 = pool.submit(slow_add, 1, 2, 3)\n",
    "```\n",
    "\n",
    "The key thing to notice here is that while the first call to `submit` is submitting a function which takes a second to run, Python is not waiting for that function to finish before moving on to the second `submit` call. They will both be submitted at almost the same time. It's not until the call to `r1.result()` that the program will wait for the `slow_add` function to finish.\n",
    "\n",
    "Most noticeably here, even though each function call took one second to run, the whole program did not take two seconds. Due to running them in parallel, it finished the whole program in just over one second."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Futures\n",
    "\n",
    "An issue with running a function asynchronously is that the return value of the function is not available immediately. This means that, when running an asynchronous function, you don’t get the return value directly. Instead, `submit` returns a placeholder for the return value. This placeholder is called a “future”, and is a variable that *in the future* will contain the result of the function.\n",
    "\n",
    "Futures are a very common variable type in parallel programming across many languages. Futures provide several common functions:\n",
    "- Block (wait) until the result is available. In `concurrent.futures`, this is done implicitly via the `.result()` function, e.g. `r1.result()` in the above script. There is also an implict wait when then context manager (the `with` block) closes to make sure all running process are finished.\n",
    "- Retrieve the result when it is available (blocking until it is available). This is also done with the `.result()` function, e.g. `r1.result()`.\n",
    "- Test whether or not the result is available. This is the `.done()` function, which returns `True` when the asynchronous function has finished and the result is available via `.result()`.\n",
    "- Test whether or not the function was a success, e.g. whether or not an exception was raised when running the function. This is the `.exception()` function, which returns the `None` if the asynchronous function completed without raising an exception and return the exception object if there was an error.\n",
    "\n",
    "In the above example, `r1` and `r2` were both futures for the results of the two asynchronous calls of `slow_sum`. The two `slow_sum` calls were processed by two worker processes. The master process was then blocked using `r1.result()` to wait for the result of the first call, and then blocked using `r2.result()` to wait for the result of the second call.\n",
    "\n",
    "We can explore this more using the following example. Create a script called `future.py` and copy into it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting future.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile future.py\n",
    "\n",
    "import time\n",
    "from concurrent.futures import ProcessPoolExecutor\n",
    "\n",
    "def slow_add(nsecs, x, y):\n",
    "    \"\"\"\n",
    "    Function that sleeps for 'nsecs' seconds, and\n",
    "    then returns the sum of x and y\n",
    "    \"\"\"\n",
    "    time.sleep(nsecs)\n",
    "    return x + y\n",
    "\n",
    "def slow_diff(nsecs, x, y):\n",
    "    \"\"\"\n",
    "    Function that sleeps for 'nsecs' seconds, and\n",
    "    then retruns the difference of x and y\n",
    "    \"\"\"\n",
    "    time.sleep(nsecs)\n",
    "    return x - y\n",
    "\n",
    "def broken_function(nsecs):\n",
    "    \"\"\"Function that deliberately raises an AssertationError\"\"\"\n",
    "    time.sleep(nsecs)\n",
    "    raise ValueError(\"Called broken function\")\n",
    "\n",
    "if __name__ == \"__main__\":\n",
    "    futures = []\n",
    "\n",
    "    with ProcessPoolExecutor() as pool:\n",
    "        futures.append(pool.submit(slow_add, 3.1, 6, 7))\n",
    "        futures.append(pool.submit(slow_diff, 2.1, 5, 2))\n",
    "        futures.append(pool.submit(slow_add, 1.1, 8, 1))\n",
    "        futures.append(pool.submit(slow_diff, 5.1, 9, 2))\n",
    "        futures.append(pool.submit(broken_function, 4.1))\n",
    "\n",
    "        while True:\n",
    "            all_finished = True\n",
    "\n",
    "            print(\"\\nHave the workers finished?\")\n",
    "\n",
    "            for i, future in enumerate(futures):\n",
    "                if future.done():\n",
    "                    print(f\"Task {i} has finished\")\n",
    "                else:\n",
    "                    all_finished = False\n",
    "                    print(f\"Task {i} is running...\")\n",
    "\n",
    "            if all_finished:\n",
    "                break\n",
    "\n",
    "            time.sleep(1)\n",
    "\n",
    "        print(\"\\nHere are the results.\")\n",
    "\n",
    "        for i, future in enumerate(futures):\n",
    "            if future.exception() is None:\n",
    "                print(f\"Task {i} was successful. Result is {future.result()}\")\n",
    "            else:\n",
    "                print(f\"Task {i} failed!\")\n",
    "                e = future.exception()\n",
    "                print(f\"    Error = {type(e)} : {e}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Running this script using should give output similar to:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "tags": [
     "nbval-ignore-output"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "Have the workers finished?\n",
      "Task 0 is running...\n",
      "Task 1 is running...\n",
      "Task 2 is running...\n",
      "Task 3 is running...\n",
      "Task 4 is running...\n",
      "\n",
      "Have the workers finished?\n",
      "Task 0 is running...\n",
      "Task 1 is running...\n",
      "Task 2 is running...\n",
      "Task 3 is running...\n",
      "Task 4 is running...\n",
      "\n",
      "Have the workers finished?\n",
      "Task 0 is running...\n",
      "Task 1 is running...\n",
      "Task 2 has finished\n",
      "Task 3 is running...\n",
      "Task 4 is running...\n",
      "\n",
      "Have the workers finished?\n",
      "Task 0 is running...\n",
      "Task 1 has finished\n",
      "Task 2 has finished\n",
      "Task 3 is running...\n",
      "Task 4 is running...\n",
      "\n",
      "Have the workers finished?\n",
      "Task 0 has finished\n",
      "Task 1 has finished\n",
      "Task 2 has finished\n",
      "Task 3 is running...\n",
      "Task 4 is running...\n",
      "\n",
      "Have the workers finished?\n",
      "Task 0 has finished\n",
      "Task 1 has finished\n",
      "Task 2 has finished\n",
      "Task 3 is running...\n",
      "Task 4 is running...\n",
      "\n",
      "Have the workers finished?\n",
      "Task 0 has finished\n",
      "Task 1 has finished\n",
      "Task 2 has finished\n",
      "Task 3 has finished\n",
      "Task 4 has finished\n",
      "\n",
      "Here are the results.\n",
      "Task 0 was successful. Result is 13\n",
      "Task 1 was successful. Result is 3\n",
      "Task 2 was successful. Result is 9\n",
      "Task 3 was successful. Result is 7\n",
      "Task 4 failed!\n",
      "    Error = <class 'ValueError'> : Called broken function\n"
     ]
    }
   ],
   "source": [
    "!venv/bin/python future.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Is this output that you expected? Note that the exception raised by `broken_function` is held safely in its associated future. This is indicated by `.exception()` not returning `None` (if you `.result()` a future that contains an exception, then that exception is raised)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "Edit the `future.py` script so that you can control the number of workers in the pool using a command line argument (e.g. using `ProcessPoolExecutor(max_workers=int(sys.argv[1]))` rather than `ProcessPoolExecutor()`).\n",
    "\n",
    "Edit the script to add calls to more asynchronous functions.\n",
    "\n",
    "Then experiment with running the script with different numbers of processes in the pool and with different numbers of asynchronous function calls.\n",
    "\n",
    "How are the asynchronous function calls distributed across the pool of worker processes?"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
